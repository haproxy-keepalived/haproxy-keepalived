cd /etc/yum.repos.d/

ls -ltr | grep -v total | awk '{print $9}' | while read f; do cp $f ${f}.backup; done

ls -ltr | egrep -v "total|backup" | awk '{print $9}' | while read f; do sed -i 's/mirrorlist/#mirrorlist/g' $f; done

ls -ltr | egrep -v "total|backup" | awk '{print $9}' | while read f; do sed -i 's|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.org|g' $f; done

# Hashed out because not working
# sed -i 's/mirrorlist/#mirrorlist/g' CentOS-Linux-*
# sed -i 's|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.org|g' CentOS-Linux-*

yum clean all
yum install -y net-tools keepalived

sleep 5

cd /etc/keepalived/
mv keepalived.conf keepalived.conf.$(date +%Y%m%d-%H%M)

PRIMARY_IP=$(cat /etc/hosts | grep ${HOSTNAME} | tail -1 | awk '{print $1}')
SECONDARY_IP=$(cat /etc/hosts | egrep -iv "${HOSTNAME}|vip|localhost" | tail -1 | awk '{print $1}')

echo ${HOSTNAME} | grep "1" > /dev/null
if [ $? -eq 0 ]; then
	MODE="MASTER"
	PRIORITY="200"
elif [ $? -ne 0 ]; then
	MODE="BACKUP"
	PRIORITY="100"
fi

cat >> keepalived.conf <<EOF
! Configuration File for keepalived

global_defs {
   notification_email {
     acassen@firewall.loc
     failover@firewall.loc
     sysadmin@firewall.loc
   }
   notification_email_from Alexandre.Cassen@firewall.loc
   smtp_server 192.168.200.1
   smtp_connect_timeout 30
   router_id LVS_DEVEL
   enable_script_security
   script_user root
   vrrp_skip_check_adv_addr
}

vrrp_script chk_haproxy {
    script "/usr/sbin/pidof haproxy"
    interval 2
}

vrrp_instance VI_1 {
    state ${MODE}
    interface eth1
    virtual_router_id 51
    priority ${PRIORITY}
    advert_int 1
    unicast_src_ip ${PRIMARY_IP}
    unicast_peer {
        ${SECONDARY_IP}
    }
    authentication {
        auth_type PASS
        auth_pass 1111
    }
    virtual_ipaddress {
        192.168.5.22/24
    }
    track_script {
        chk_haproxy
    }
}
EOF

systemctl daemon-reload
systemctl enable keepalived
systemctl start keepalived